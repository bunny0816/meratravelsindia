@extends('layout/master_layout')
@section('bodyContent')

<!--==========================
      Contact Section
    ============================-->
     <section id="contact-header" class="wow fadeIn contact-header">     
    </section>
   <section id="contact" class="section-bg wow fadeInUp">
   
      <div class="container">

        <div class="section-header contact-top">
          <h3>Contact Us</h3>
          <h2 style="text-align: center;">Get in touch with us......</h2>
        </div>

        <div class="row contact-info">

          <div class="col-md-4">
            <div class="contact-address">
              <i class="ion-ios-location-outline"></i>
              <h3>Address</h3>
              <address>Plot No.2, Asma Complex,<br>
              A-Sector, Sarvadharam Colony,<br>
              Kolar Road, Bhopal - 462042 </address>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="ion-ios-telephone-outline"></i>
              <h3>Phone Number</h3>
              <p><a href="tel:+91-9826023445"> +91-9826023445</a></p>
              <p><a href="tel:+91-9977158989"> +91-9977158989</a></p>
              <p><a href="tel:+91-755-4252902"> +91-755-4252902</a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-email">
              <i class="ion-ios-email-outline"></i>
              <h3>Email</h3>
              <p><a href="mailto:meratravelsbpl@gmail.com"> meratravelsbpl@gmail.com</a></p>
            </div>
          </div>

        </div>

        <div class="form">
          <div id="sendmessage">Your message has been sent. Thank you!</div>
          <div id="errormessage"></div>
          <form action="" method="post" role="form" class="contactForm">
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
              <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
              <div class="validation"></div>
            </div>
            <div class="text-center"><button type="submit">Send Message</button></div>
          </form>
        </div>

      </div>
    </section><!-- #contact --><section id="contact-map" class="section-bg wow fadeIn">
         <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3667.4810671205155!2d77.41460901428567!3d23.189132615903!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x397c430c21326d4f%3A0x8f780a1fe2adcc2e!2sShruti%20Apartment%2C%20Kolar%20Rd%2C%20Sarvdharm%20Colony%2C%20Kolar%20Rd%2C%20Bhopal%2C%20Madhya%20Pradesh%20462007!5e0!3m2!1sen!2sin!4v1569153209919!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
      
    </section><!-- #call-to-action -->

@endsection