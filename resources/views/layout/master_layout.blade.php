<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Mera Travels | A Faithful Travel Partner</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="flight, flights, flight booking, airfare, air tickets, cheap air tickets, flight booking, flight tickets, flight ticket booking, lowest airfares, air flight booking, cheap flight ticket, cheap flights, air travel" name="keywords">
  <meta content="" name="description">
  <meta name="theme-color" content="#0a0c1173" />
  
  <!-- Favicons -->
  <link href="resources/img/favicon.png" rel="icon">
  <link href="resources/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="resources/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="resources/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="resources/lib/animate/animate.min.css" rel="stylesheet">
  <link href="resources/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="resources/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="resources/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="resources/css/style.css" rel="stylesheet">

</head>

<body>

  <!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container-fluid">

      <div id="logo" class="pull-left">
        <!-- <a href="#intro"><img src="resources/img/logo/logo1.png" alt="" title="" width="120px" style="margin-top: -31px" /></a> -->
       <h1><a href="#intro" class="scrollto">Mera Travels</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
       <!--  <a href="#intro"><img src="resources/img/logo/mera_travels.png" height="100px" width="100px" alt="" title="" /></a> -->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="{{URL::to('/')}}">Home</a></li>
          <li><a href="#about">About Us</a></li>
          <!-- <li><a href="#services">Services</a></li> -->
          <li class="menu-has-children"><a href="">Destination</a>
            <ul>
              <li><a href="#">Indian</a></li>
              <li><a href="#">International</a></li>              
            </ul>
          </li>
           <li class="menu-has-children"><a href="">Tour</a>
            <ul>
              <li><a href="#">Adventure</a></li>
              <li><a href="#">Religious</a></li>
              <li><a href="#">Family</a></li>              
            </ul>
          </li>
          <li><a href="{{URL::to('/contact')}}">Contact</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

  <!--==========================
   Content Area
  ============================-->

  @yield('bodyContent')

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
           <h3>Mera Travels</h3>            
            <a href="#intro"><img src="resources/img/logo/logo1.png" alt="" title="" width="75%" /></a>
          </div>
 
          <!-- <div class="col-lg-3 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">Home</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">About us</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">Services</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div> -->

          <div class="col-lg-4 col-md-6 footer-contact">
            <h4>Contact Us</h4>
            <p>Plot No.2, Asma Complex,<br>
              A-Sector, Sarvadharam Colony,<br>
              Kolar Road, Bhopal - 462042 <br>
              <strong>Mobile:</strong><a href="tel:+91-9826023445"> +91-9826023445</a><br>
              <strong>Mobile:</strong><a href="tel:+91-9977158989"> +91-9977158989</a><br>
              <strong>Phone:</strong><a href="tel:+91-755-4252902"> +91-755-4252902</a><br>
              <strong>Email:</strong><a href="mailto:meratravelsbpl@gmail.com"> meratravelsbpl@gmail.com</a><br>
            </p>

            <div class="social-links">
              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>              
            </div>

          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>Subscribe for our latest tour packages and offers.</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit"  value="Subscribe">
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Mera Travels</strong>. All Rights Reserved
      </div>      
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
 <div id="preloader"></div>

  <!-- JavaScript Libraries -->
  <script src="resources/lib/jquery/jquery.min.js"></script>
  <script src="resources/lib/jquery/jquery-migrate.min.js"></script>
  <script src="resources/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="resources/lib/easing/easing.min.js"></script>
  <script src="resources/lib/superfish/hoverIntent.js"></script>
  <script src="resources/lib/superfish/superfish.min.js"></script>
  <script src="resources/lib/wow/wow.min.js"></script>
  <script src="resources/lib/waypoints/waypoints.min.js"></script>
  <script src="resources/lib/counterup/counterup.min.js"></script>
  <script src="resources/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="resources/lib/isotope/isotope.pkgd.min.js"></script>
  <script src="resources/lib/lightbox/js/lightbox.min.js"></script>
  <script src="resources/lib/touchSwipe/jquery.touchSwipe.min.js"></script>
  <!-- Contact Form JavaScript File -->
  

  <!-- Template Main Javascript File -->
  <script src="resources/js/main.js"></script>

</body>
</html>